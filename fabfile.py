from fabric.api import cd, sudo, shell_env, run, env, settings

HOST_PATH = "/opt/django/microblog"

ex_env = {
    "DJANGO_SETTINGS_MODULE": 'microblog.settings.prod',
    # "DJANGO_SETTINGS_MODULE": "microblog.settings.dev",
}

# env.hosts = ["192.168.11.6", ]
env.hosts = ["192.168.33.10", ]
env.user = "vagrant"
# env.sudo_user = "vagrant"
env.password = "vagrant"

env.use_ssh_config = True


def collect_static():
    with cd(HOST_PATH):
        with shell_env(**ex_env):
            sudo("/opt/django/venv/bin/python manage.py collectstatic --no-input")
            sudo("echo $?")


def git_update():
    with cd(HOST_PATH):
        run("git pull")

def kill():
    sudo("pkill -f venv")


def start():
    with cd(HOST_PATH):
        with shell_env(**ex_env):
            # sudo("kill -9 28653")
            sudo("/opt/django/venv/bin/gunicorn -c gunicorn.conf.py microblog.wsgi")
            # sudo("/opt/django/venv/bin/gunicorn microblog.wsgi")

def restart():
    kill()
    start()

def all():
    git_update()
    collect_static()
    kill()
    start()
